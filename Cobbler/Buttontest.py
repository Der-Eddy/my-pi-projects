#!/usr/bin/env python

from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN)
GPIO.setup(18, GPIO.OUT)

while True:
	if ( GPIO.input(23) == False ):
		GPIO.output(18, True)
		print 'True'
	else:
		GPIO.output(18, False)
		print 'False'
	sleep(1);

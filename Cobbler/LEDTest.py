
#!/usr/bin/env python

import RPi.GPIO as GPIO, time

GPIO.setmode(GPIO.BCM)
LCD_RS = 25
LCD_E  = 24
LCD_D4 = 23 
LCD_D5 = 17
LCD_D6 = 27
LCD_D7 = 22
GPIO.setup(LCD_RS, GPIO.OUT)
GPIO.setup(LCD_E, GPIO.OUT)
GPIO.setup(LCD_D4, GPIO.OUT)
GPIO.setup(LCD_D5, GPIO.OUT)
GPIO.setup(LCD_D6, GPIO.OUT)
GPIO.setup(LCD_D7, GPIO.OUT)

GPIO.output(LCD_RS, True)
GPIO.output(LCD_E, True)
GPIO.output(LCD_D4, True)
GPIO.output(LCD_D5, True)
GPIO.output(LCD_D6, True)
GPIO.output(LCD_D7, True)

raw_input("Press ENTER ...")

GPIO.output(LCD_RS, False)
GPIO.output(LCD_E, False)
GPIO.output(LCD_D4, False)
GPIO.output(LCD_D5, False)
GPIO.output(LCD_D6, False)
GPIO.output(LCD_D7, False)

GPIO.cleanup()
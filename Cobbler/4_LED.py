#!/usr/bin/env python

import RPi.GPIO as GPIO, time

GPIO.setmode(GPIO.BCM)
LED1 = 18
LED2 = 23
LED3 = 24
LED4 = 25
GPIO.setup(LED1, GPIO.OUT)
GPIO.setup(LED2, GPIO.OUT)
GPIO.setup(LED3, GPIO.OUT)
GPIO.setup(LED4, GPIO.OUT)
TIME = 1

while True:
	GPIO.output(LED1, True)
	GPIO.output(LED4, False)
	time.sleep(TIME)
	GPIO.output(LED2, True)
	GPIO.output(LED1, False)
	time.sleep(TIME)
	GPIO.output(LED3, True)
	GPIO.output(LED2, False)
	time.sleep(TIME)
	GPIO.output(LED4, True)
	GPIO.output(LED3, False)
	time.sleep(TIME)